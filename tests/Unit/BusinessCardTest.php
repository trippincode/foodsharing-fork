<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Attribute\DataProvider;
use Codeception\Test\Unit;
use Foodsharing\Modules\BusinessCard\BusinessCardControl;
use ReflectionClass;
use Tests\Support\UnitTester;

class BusinessCardTest extends Unit
{
    protected UnitTester $tester;

    private BusinessCardControl $business_card;

    public function _before()
    {
        $this->business_card = $this->tester->get(BusinessCardControl::class);
    }

    protected function _after()
    {
    }

    #[DataProvider('pageProvider')]
    public function testPositionStreetNumber(string $address, int $index): void
    {
        $out = $this->invokePrivateMethod([$address]);
        $this->assertEquals(
            $index,
            $out
        );
    }

    /**
     * Helper method for accessing the private function 'index_of_first_number'.
     */
    private function invokePrivateMethod(array $parameters = [])
    {
        $reflection = new ReflectionClass($this->business_card::class);
        $method = $reflection->getMethod('index_of_first_number');

        return $method->invokeArgs($this->business_card, $parameters);
    }

    /**
     * Provides the test data for {@see testPositionStreetNumber}.
     */
    public static function pageProvider(): array
    {
        return [
            ['Straße 1', 7],
            ['Sträßchän 1', 10],
            ['Land Landggg Langggg adsflkja dsfölkajsd flökasjdf dddGünter-Lechner-Allee 111', 75],
            ['Teststraße-∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫-abcdefgxyz 123', 42],
            ['Teststraße-∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫∫-abcdefgxyzabcdefghijklmnopq 123', 59]
        ];
    }
}
