<?php

// table StoreSettings

namespace Foodsharing\Modules\Core\DBConstants\Store;

class StoreSettings
{
    final public const USE_PICKUP_RULE_YES = 1;
    final public const USE_PICKUP_RULE_NO = 0;
    final public const PRESS_YES = 1;
    final public const PRESS_NO = 0;
}
