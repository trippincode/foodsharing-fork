<?php

// table fs_answers

namespace Foodsharing\Modules\Core\DBConstants\Quiz;

/**
 * Table field `right`.
 */
class AnswerRating
{
    final public const WRONG = 0;
    final public const CORRECT = 1;
    final public const NEUTRAL = 2;
}
