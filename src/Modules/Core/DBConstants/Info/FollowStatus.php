<?php

// table fs_theme_follower

namespace Foodsharing\Modules\Core\DBConstants\Info;

/**
 * Following status for forum threads.
 */
class FollowStatus
{
    final public const DISABLED = 0;
    final public const ENABLED = 1;
}
