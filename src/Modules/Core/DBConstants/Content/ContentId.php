<?php

namespace Foodsharing\Modules\Core\DBConstants\Content;

/*
 *
 * At the beginning of this class there were up to 49 content ids given as number
 * please replace them by constants to make them easier to read.
 *
 * You can find them by searching for the use of `$this->contentGateway->get()`
 * */

class ContentId
{
    final public const PARTNER_PAGE_10 = 10;
    final public const PARTNER_PAGE_AU_79 = 79;
    final public const QUIZ_DESCRIPTION_PAGE_12 = 12;
    final public const QUIZ_FAILED_PAGE_13 = 13;
    final public const QUIZ_CONFIRM_FS_PAGE_14 = 14;
    final public const QUIZ_CONFIRM_SM_PAGE_15 = 15;
    final public const QUIZ_CONFIRM_AMB_PAGE_16 = 16;
    final public const QUIZ_START_PAGE_17 = 17;
    final public const QUIZ_POPUP_PAGE_18 = 18;
    final public const QUIZ_FAILED_FS_TRY_1_PAGE_19 = 19;
    final public const QUIZ_FAILED_FS_TRY_2_PAGE_20 = 20;
    final public const QUIZ_FAILED_FS_TRY_3_PAGE_21 = 21;
    final public const QUIZ_FAILED_SM_TRY_1_PAGE_22 = 22;
    final public const QUIZ_FAILED_SM_TRY_2_PAGE_23 = 23;
    final public const QUIZ_FAILED_SM_TRY_3_PAGE_24 = 24;
    final public const QUIZ_FAILED_AMB_TRY_1_PAGE_25 = 25;
    final public const QUIZ_FAILED_AMB_TRY_2_PAGE_26 = 26;
    final public const QUIZ_FAILED_AMB_TRY_3_PAGE_27 = 27;
    final public const QUIZ_REMARK_PAGE_33 = 33;
    final public const QUIZ_POPUP_SM_PAGE_34 = 34;
    final public const QUIZ_MESSAGE_SM_UNVERIFIED_PAGE_45 = 45;
    final public const QUIZ_POPUP_AMB_PAGE_35 = 35;
    final public const QUIZ_POPUP_AMB_LAST_PAGE_36 = 36;
    final public const QUIZ_LEGAL_FOODSAVER = 30;
    final public const QUIZ_LEGAL_STOREMANAGER = 31;

    final public const PRIVACY_POLICY_CONTENT = 28;
    final public const TEAM_HEADER_PAGE_39 = 39;
    final public const TEAM_ACTIVE_PAGE_53 = 53;
    final public const TEAM_FORMER_ACTIVE_PAGE_54 = 54;
    final public const PRIVACY_NOTICE_CONTENT = 64;
    final public const BROADCAST_MESSAGE = 51;

    final public const STARTPAGE_BLOCK1_DE = 80;
    final public const STARTPAGE_BLOCK2_DE = 81;
    final public const STARTPAGE_BLOCK3_DE = 82;

    final public const STARTPAGE_BLOCK1_BETA = 83;
    final public const STARTPAGE_BLOCK2_BETA = 84;
    final public const STARTPAGE_BLOCK3_BETA = 85;

    final public const STARTPAGE_BLOCK1_AT = 86;
    final public const STARTPAGE_BLOCK2_AT = 87;
    final public const STARTPAGE_BLOCK3_AT = 88;

    final public const STARTPAGE_BLOCK1_CH = 89;
    final public const STARTPAGE_BLOCK2_CH = 90;
    final public const STARTPAGE_BLOCK3_CH = 91;

    final public const PRESS = 58;
    final public const COMMUNITIES_GERMANY = 52;
    final public const COMMUNITIES_AUSTRIA = 61;
    final public const COMMUNITIES_SWITZERLAND = 62;
    final public const DEMANDS = 60;
    final public const CONTACT = 73;
    final public const ACADEMY = 69;
    final public const FESTIVAL = 72;
    final public const INTERNATIONAL = 74;
    final public const TRANSPARENCY = 68;
    final public const PAST_CAMPAIGNS = 46;
    final public const RESCUE_FOOD_SHARE_POINT = 49;
    final public const IMPRINT = 8;
    final public const ABOUT = 9;
    final public const FOODSHARING_CITIES = 66;
    final public const FOR_COMPANIES = 4;
    final public const WORKSHOPS = 71;
    final public const STATISTICS_PAGE = 11;
    final public const SECURITY_PAGE = 59;
}
