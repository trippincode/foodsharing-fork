<?php

namespace Foodsharing\Modules\Core\DBConstants\Map;

class MapConstants
{
    final public const CENTER_GERMANY_LAT = 50.89;
    final public const CENTER_GERMANY_LON = 10.13;
    final public const ZOOM_COUNTRY = 6;
    final public const ZOOM_CITY = 13;
}
